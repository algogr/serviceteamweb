package gr.algo.ServiceTeamWeb


import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

@Entity
@Table

data class Findoc(
    @Id @GeneratedValue
    val findoc: Int=-1,
    @Column
    val fincode:String?=null,
    @Column
    var ccckatastash: Int?=null,
    @Column
    var ccccloseddatetime: LocalDateTime?=null,
    @Column
    var cccappointmentfrom:LocalDateTime?=null,
    @Column
    var ccctexnikos: Int?=null,
    @Column(insertable=false, updatable=false)
    val cccsubscriber:Int?=null,

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="cccsubscriber")
    val subscriber:Subscriber

)