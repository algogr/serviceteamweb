package gr.algo.ServiceTeamWeb

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository

interface SubscriberRepository:JpaRepository<Subscriber,Int>{

}