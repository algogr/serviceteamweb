package gr.algo.ServiceTeamWeb

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository

interface FindocRepository:JpaRepository<Findoc,Int>{
    fun findByfincode(fincode:String):Findoc

}