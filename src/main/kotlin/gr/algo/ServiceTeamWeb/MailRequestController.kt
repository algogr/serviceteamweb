package gr.algo.ServiceTeamWeb


import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


@Controller
class MailRequestController{

    @Autowired
    lateinit var findocRepository: FindocRepository

    @GetMapping("/complete")
    fun completeTask(@RequestParam im:String,model:Model):String

    {
        var ticket=findocRepository.findByfincode(im)
        ticket.ccckatastash=2
        ticket.ccccloseddatetime= LocalDateTime.now()
        findocRepository.save(ticket)
        //@Query(value="SELECT devname,hrs,ot FROM imaginaryTable",nativeQuery=true)

        model.addAttribute("IM",im)
        model.addAttribute("COMMENT","ολοκληρώθηκε!!!")
        return "response"

    }

    @GetMapping("/cancel")
    fun cancelTask(@RequestParam im:String,model:Model):String{
        var ticket=findocRepository.findByfincode(im)
        ticket.ccckatastash=3
        findocRepository.save(ticket)
        model.addAttribute("IM",im)
        model.addAttribute("COMMENT","ακυρώθηκε!!!")
        return "response"
    }


    @GetMapping("/defer")
    fun deferTask(@RequestParam im:String,model:Model):String

    {

        model.addAttribute("IM",im)

        return "defer"

    }

    @GetMapping("/deferfinal")
    fun deferFinalTask(@RequestParam im:String,@RequestParam ndatetime:String,model:Model):String

    {
        var ticket=findocRepository.findByfincode(im)
        ticket.ccckatastash=4
        ticket.ccctexnikos=null
        ticket.cccappointmentfrom=LocalDateTime.parse(ndatetime, DateTimeFormatter.ISO_DATE_TIME)
        findocRepository.save(ticket)
        //@Query(value="SELECT devname,hrs,ot FROM imaginaryTable",nativeQuery=true)

        model.addAttribute("IM",im)
        model.addAttribute("COMMENT","μετατέθηκε για ${ndatetime}!!!")
        return "response"

    }

    @GetMapping("/tickets")
    fun getTickets(@RequestParam tech:String,@RequestParam status:String)
    {

    }



}

