package gr.algo.ServiceTeamWeb

import javax.persistence.*


@Entity
@Table(name="cccsubscriber")

data class Subscriber(
        @Id
        val cccsubscriber:Int=-1,

        val name:String="",
        @Column(name="phone01")
        val phone1:String="",
        @Column(name="phone02")
        val phone2:String="",
        @Column(name="cccphone02")
        val phone3:String="",
        @Column(name="cccnomos")
        val nomos:String="",
        val city:String="",
        val address:String="",
        @Column(name="cccperioxi")
        val location:String="",

        @OneToOne(fetch = FetchType.LAZY,
                mappedBy = "subscriber")
        val findoc: Findoc


)