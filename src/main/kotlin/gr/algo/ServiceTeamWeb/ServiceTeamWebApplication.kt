package gr.algo.ServiceTeamWeb

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.apache.tomcat.jni.SSL.setPassword
import org.springframework.jdbc.datasource.DriverManagerDataSource



@SpringBootApplication
class ServiceTeamWebApplication

fun main(args: Array<String>) {
    runApplication<ServiceTeamWebApplication>(*args)
}

